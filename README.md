# What is this
This is a demo of [Giosg reporting api](http://developers.giosg.com/reporting_http_api.html#chat-stats-api)

# How to make local build
You need to have nodejs installed, at least version 8.
```
npm install
# npm run lint
npm run build
```
After this built project will be inside `/public` folder

# How to run local devserver (http://localhost:4000)
```
npm install
npm start
```