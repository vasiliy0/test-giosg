import "babel-polyfill";

import * as React from "react";
import * as ReactDOM from "react-dom";

import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import { Root } from "./root";

const root = document.getElementById("root");
if (!root) {
    throw new Error("No root element!");
}

console.info("Mounting main component");
ReactDOM.render(<Root />, root);
