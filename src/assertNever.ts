/** Usefull thing to check at complication time that all union cases are checked */
export default function assertNever(x: never): never {
    throw new Error(`Unexpected object: ${x}`);
}
