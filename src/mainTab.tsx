import * as React from "react";
import { Form, FormGroup, Label, Input } from "reactstrap";
import { DailyStatsInfo } from "./dailyStatsInfo";


/*
    Maybe in the future it will be good to move all "tabs" components into their own folders
*/

// Actually to use localStorage is not a good idea. What if user will want to store that data on server-side?
//  This will lead to use async methods, and this probably will lead to some code update

const LS_FROM_DATE_KEY = "fromDate";
const LS_TO_DATE_KEY = "toDate";
const LS_TOKEN_KEY = "token";
const LS_ROOMID_KEY = "roomId";

interface MainInputsState {
    fromDate: string;
    toDate: string;
    token: string;
    roomId: string;
}
/** Inputs and data representation */
export class MainTab extends React.Component<{}, MainInputsState> {
    state: MainInputsState = {
        fromDate: localStorage.getItem(LS_FROM_DATE_KEY) || "2017-05-01",
        toDate: localStorage.getItem(LS_TO_DATE_KEY) || "2017-06-15",
        token: localStorage.getItem(LS_TOKEN_KEY) || "",
        roomId:
            localStorage.getItem(LS_ROOMID_KEY) ||
            "84e0fefa-5675-11e7-a349-00163efdd8db"
    };
    render() {
        return (
            <div className="mt-2">
                <form className="mb-3">
                    <div className="d-flex flex-column flex-md-row">
                        <div className="form-group mr-1">
                            <label>Start date</label>
                            <input
                                type="date"
                                className="form-control"
                                placeholder=""
                                value={this.state.fromDate}
                                onChange={e => {
                                    this.setState({
                                        fromDate: e.target.value
                                    });
                                    localStorage.setItem(
                                        LS_FROM_DATE_KEY,
                                        e.target.value
                                    );
                                }}
                            />
                        </div>
                        <div className="form-group mr-1">
                            <label>End date</label>
                            <input
                                type="date"
                                className="form-control"
                                placeholder=""
                                value={this.state.toDate}
                                onChange={e => {
                                    this.setState({
                                        toDate: e.target.value
                                    });
                                    localStorage.setItem(
                                        LS_TO_DATE_KEY,
                                        e.target.value
                                    );
                                }}
                            />
                        </div>

                        <div className="form-group ml-md-auto mr-1">
                            <label>Security token</label>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">
                                        <i className="fa fa-key fa-fw" />
                                    </div>
                                </div>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Enter token here"
                                    value={this.state.token}
                                    onChange={e => {
                                        this.setState({
                                            token: e.target.value
                                        });
                                        localStorage.setItem(
                                            LS_TOKEN_KEY,
                                            e.target.value
                                        );
                                    }}
                                />
                            </div>
                        </div>

                        <div className="form-group mr-1">
                            <label>Room id</label>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <div className="input-group-text">
                                        <i className="fa fa-cubes fa-fw" />
                                    </div>
                                </div>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Enter roomid here"
                                    value={this.state.roomId}
                                    onChange={e => {
                                        this.setState({
                                            roomId: e.target.value
                                        });
                                        localStorage.setItem(
                                            LS_ROOMID_KEY,
                                            e.target.value
                                        );
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </form>

                <DailyStatsInfo
                    key={`${this.state.fromDate}#${this.state.toDate}#${
                        this.state.roomId
                    }#${this.state.token}`}
                    {...this.state}
                />
            </div>
        );
    }
}
