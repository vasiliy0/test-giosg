export const DATES_RANGE_MAX_DAYS = 180;

export type RoomId = string;
export type DailyChatReport = Readonly<{
    room_id: RoomId;
    start_date: string;
    end_date: string;
    total_visitors_with_conversation_count: number;
    total_visitors_with_chat_count: number;
    total_visitors_affected_by_chat_count: number;
    total_visitors_autosuggested_count: number;
    total_chats_from_autosuggest_count: number;
    total_chats_from_user_count: number;
    total_chats_from_visitor_count: number;
    /** Count of unique chat sessions that were considered as proper conversations in given time range. See definition of “Conversation” below. */
    total_conversation_count: number;
    /** Total count of visitor messages sent in given time range. */
    total_visitor_message_count: number;
    /** Total count of user chat messages sent in given time range. */
    total_user_message_count: number;
    missed_chat_count: number;
    by_date: ReadonlyArray<DailyChatData>;
}>;

export type DailyChatData = Readonly<{
    /** Date of this data object. The value is ISO 8601 date string, e.g. 2016-01-01 */
    date: string;
    /** Count of unique daily visitors that had proper conversation at that day. See definition of “Conversation” below. */
    visitors_with_conversation_count: number;
    visitors_with_chat_count: number; //	Count of unique daily visitors that had chat at that day. This includes all chats where there is at least one visitor message.
    visitors_affected_by_chat_count: number; //	Count of unique daily visitors that were affected by chat at that day. See definition of affected by chat below.
    visitors_autosuggested_count: number; //	Count of unique daily visitors that were autosuggested at that day.
    chats_from_autosuggest_count: number; //	Count of chat sessions started by autosuggest message at that day.
    chats_from_user_count: number; //	Count of chat sessions started by user manually sending message at that day.
    chats_from_visitor_count: number; //	Count of chat sessions started by visitor message at that day.
    /** Count of unique chat sessions that were considered as proper conversations at that day. See definition of “Conversation” below. */
    conversation_count: number;
    visitor_message_count: number; //	Total count of visitor messages sent at that day.
    user_message_count: number; //	Total count of user chat messages sent at that day.
    /** Count of unique chat sessions at that day that were missed by users. This means that there were visitor messages but no responses from users. */
    missed_chat_count: number;
}>;

export function chatDailyStats(opts: {
    token: string;
    roomId: string;
    startDate: string;
    endDate: string;
}) {
    // API url in task description is deprecated
    // Not unsing encodeUriComponent because all params will be not altered after enconding
    const url =
        `https://api.giosg.com/api/reporting/v1/rooms/${
            opts.roomId
        }/visitorchatstats/daily/` +
        `?start_date=${opts.startDate}&end_date=${opts.endDate}` +
        `&token=${opts.token}`; // What's this? This is my guess about pre-flight request issue

    return fetch(url, {
        method: "GET",
        headers: new Headers({
            Authorization: `Token ${opts.token}`,
            Accept: "application/json"
        })
    }).then(x => x.json()) as Promise<DailyChatReport>;

    // Here is commented data generator. Was used to test user interface
    /*
    const daysDiff = Math.max(moment(opts.endDate).diff(moment(opts.startDate), "days") + 1, 0);
    
    // a fake until token will be received
    return new Promise<DailyChatReport>(resolve => setTimeout(() => resolve({
        room_id: opts.roomId,
        start_date: opts.startDate,
        end_date: opts.endDate,
        total_visitors_with_conversation_count: 0,
        total_visitors_with_chat_count: 0,
        total_visitors_affected_by_chat_count: 0,
        total_visitors_autosuggested_count: 0,
        total_chats_from_autosuggest_count: 0,
        total_chats_from_user_count: 0,
        total_chats_from_visitor_count: 0,
        total_conversation_count: daysDiff * 100,
        total_visitor_message_count: daysDiff * 256,
        total_user_message_count: daysDiff * 512,
        missed_chat_count: 0,
        by_date: Array(
            daysDiff
        )
            .fill(0)
            .map((_, id) => ({
                date: moment(opts.startDate)
                    .add(id, "days")
                    .format("YYYY-MM-DD"),
                visitors_with_conversation_count: Math.floor(Math.sin(id * 5) * 10 + 30),
                visitors_with_chat_count: 0,
                visitors_affected_by_chat_count: 0,
                visitors_autosuggested_count: 0,
                chats_from_autosuggest_count: 0,
                chats_from_user_count: 0,
                chats_from_visitor_count: 0,
                conversation_count: Math.floor(Math.sin(id * 20) * 20 + 20),
                visitor_message_count: 0,
                user_message_count: 0,
                missed_chat_count: Math.floor(Math.sin(id * 10) * 10 + 10),
            }))
    }), 500));
    */
}
