import * as React from "react";
import { Navbar, NavbarBrand, Container } from "reactstrap";
import { MainTab } from "./mainTab";
import "./webpackBuildVersion";

/** Navbar and container with main tab */
export class Root extends React.Component<{}, {}> {
    /* In the future there will be some state controller used. As for me, MobX is a good choice */
    render() {
        return (
            <>
                {/* In the future navbar should have tabs to select separate pages of the app */}
                <Navbar color="dark" dark expand="md">
                    <NavbarBrand href="/">Demo app</NavbarBrand>
                </Navbar>
                <Container>
                    {/* In the future here will be something like HashRouter to show different tabs */}
                    <MainTab/>
                </Container>
                <div className="my-3 text-center"><small>Built at {new Date(__VERSION__).toLocaleString()}</small></div>
            </>
        );
    }
}
