import * as React from "react";
import { DailyChatReport, chatDailyStats, DailyChatData, DATES_RANGE_MAX_DAYS } from "./giosg-sdk";
import assertNever from "./assertNever";
import classnames from "classnames";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from "recharts";
import moment from "moment";

const ITEMS_ON_PAGE = 5;

interface DailyStatsInfoState {
    error?: Error;
    data?: DailyChatReport;
    pageId: number;
    sortColumn: keyof DailyChatData;
    sortDesc: boolean;
}
interface DailyStatsInfoProps {
    fromDate: string;
    toDate: string;
    token: string;
    roomId: string;
}
/**
 * This component load and shows daily data.
 * It must be rendered with "key" with all props, because overwise it will show old data
 */
export class DailyStatsInfo extends React.Component<
    DailyStatsInfoProps,
    DailyStatsInfoState
> {
    /*
     To show the actual data from the props a "key" method is used.
     It is simple, so it leaves less room to make a mistake.
     Ways like getDerivedStateFromProps or componentDidUpdate are possible, but not
       required here. It is ok to re-create that component if props are changed.
    */

    mounted = false; // Safeguard to not to update state on unmonted component
    state: DailyStatsInfoState = {
        pageId: 0,
        sortColumn: "date",
        sortDesc: false
    };
    componentDidMount() {
        this.mounted = true;
        if (this.props.token && this.props.fromDate && this.props.toDate && this.props.roomId) {
            chatDailyStats({
                startDate: this.props.fromDate,
                endDate: this.props.toDate,
                token: this.props.token,
                roomId: this.props.roomId,
            })
                .then(
                    data =>
                        this.mounted
                            ? this.setState({
                                  data
                              })
                            : undefined
                )
                .catch(
                    e =>
                        this.mounted
                            ? this.setState({
                                  error:
                                      e instanceof Error ? e : new Error(`${e}`)
                              })
                            : undefined
                );
        }
    }
    componentWillUnmount() {
        this.mounted = false;
    }
    render() {
        const data = this.state.data;
        function errorInfo(str: string) {
            return (
                <div className="text-center text-warning">
                    <i className="fa fa-times fa-fw" />
                    {str}
                </div>
            );
        }

        if (!this.props.token) {
            return errorInfo("No token");
        } else if (!this.props.fromDate) {
            return errorInfo("No start date");
        } else if (!this.props.toDate) {
            return errorInfo("No end date");
        } else if (!this.props.roomId) {
            return errorInfo("No room id");
        }
        const daysDiff =
            moment(this.props.toDate).diff(
                moment(this.props.fromDate),
                "days"
            ) + 1;
        if (daysDiff <= 0) {
            return errorInfo(`Time range is inversed!`);
        } else if (daysDiff > DATES_RANGE_MAX_DAYS) {
            // Maybe no need to inform user about this? Maybe fetch all data by ranges?
            return errorInfo(`Time range is too big for API`);
        }
        if (this.state.error) {
            return errorInfo(
                `${this.state.error.name}: ${this.state.error.message}`
            );
        } else if (!data) {
            return (
                <div className="text-center">
                    <i className="fa fa-spin fa-spinner fa-fw" />Loading
                </div>
            );
        }

        const columnName = (column: keyof DailyChatData) =>
            column === "date"
                ? "Date"
                : column === "conversation_count"
                    ? "Conversations"
                    : column === "missed_chat_count"
                        ? "Missed chats"
                        : column === "visitors_with_conversation_count"
                            ? "Visitors with conversations"
                            : "UNKNOWN";
        const tableHeader = (column: keyof DailyChatData) => (
            <th
                key={column}
                onClick={() =>
                    this.setState({
                        sortColumn: column,
                        sortDesc:
                            this.state.sortColumn === column
                                ? !this.state.sortDesc
                                : false
                    })
                }
                style={{
                    cursor: "pointer",
                    overflowY: "hidden"
                }}
            >
                <span className="d-none d-sm-inline">{columnName(column)}</span>
                <span className="d-sm-none">
                    {columnName(column).slice(0, 3)}
                </span>
                {this.state.sortColumn === column ? (
                    <i
                        className={classnames(
                            "fa fa-fw",
                            this.state.sortDesc ? "fa-sort-up" : "fa-sort-down"
                        )}
                        onClick={() =>
                            this.setState({ sortDesc: !this.state.sortDesc })
                        }
                    />
                ) : (
                    <i className="fa fa-fw" />
                )}
            </th>
        );


        const pagesCount = Math.ceil(data.by_date.length / ITEMS_ON_PAGE);

        return (
            <div>
                <div className="card-deck mb-4">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title text-center">
                                {data.total_conversation_count}
                            </h5>
                            <p className="card-text text-center">
                                Total conversation count
                            </p>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title text-center">
                                {data.total_user_message_count}
                            </h5>
                            <p className="card-text text-center">
                                Total user message count
                            </p>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title text-center">
                                {data.total_visitor_message_count}
                            </h5>
                            <p className="card-text text-center">
                                Total visitor message count
                            </p>
                        </div>
                    </div>
                </div>
                <div className="mb-4">
                    <ResponsiveContainer width={"100%"} height={200}>
                        <LineChart
                            data={data.by_date.map(x => ({
                                date: moment(x.date).format("MMM, D"),
                                conversation_count: x.conversation_count,
                                missed_chat_count: x.missed_chat_count,
                                visitors_with_conversation_count:
                                    x.visitors_with_conversation_count
                            }))}
                        >
                            <XAxis dataKey="date" />
                            <YAxis width={20} />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Line
                                type="monotone"
                                dataKey="conversation_count"
                                stroke="#8884d8"
                                name={columnName("conversation_count")}
                            />
                            <Line
                                type="monotone"
                                dataKey="missed_chat_count"
                                stroke="#CC0000"
                                name={columnName("missed_chat_count")}
                            />
                            <Line
                                type="monotone"
                                dataKey="visitors_with_conversation_count"
                                stroke="green"
                                name={columnName(
                                    "visitors_with_conversation_count"
                                )}
                            />
                        </LineChart>
                    </ResponsiveContainer>
                </div>
                <div className="mb-2">
                    <table className="table">
                        <thead>
                            <tr>
                                {tableHeader("date")}
                                {tableHeader("conversation_count")}
                                {tableHeader("missed_chat_count")}
                                {tableHeader(
                                    "visitors_with_conversation_count"
                                )}
                            </tr>
                        </thead>
                        <tbody>
                            {data.by_date
                                .slice() // making clone because sort is mutating
                                /* 
                                
                                Actually sorting and paging are usually performed at back-end side.                                
                                
                                */
                                .sort((aRow, bRow) => {
                                    const a = aRow[this.state.sortColumn];
                                    const b = bRow[this.state.sortColumn];

                                    return this.state.sortDesc
                                        ? a < b
                                            ? 1
                                            : a > b
                                                ? -1
                                                : 0
                                        : a < b
                                            ? -1
                                            : a > b
                                                ? 1
                                                : 0;
                                })
                                .slice(
                                    this.state.pageId * ITEMS_ON_PAGE,
                                    this.state.pageId * ITEMS_ON_PAGE +
                                        ITEMS_ON_PAGE
                                )
                                .map((item, idx) => (
                                    <tr key={`${item.date}-${idx}`}>
                                        <td>
                                            {moment(item.date).format("MMM, D")}
                                        </td>
                                        <td>{item.conversation_count}</td>
                                        <td>{item.missed_chat_count}</td>
                                        <td>
                                            {
                                                item.visitors_with_conversation_count
                                            }
                                        </td>
                                    </tr>
                                ))}
                        </tbody>
                    </table>
                </div>
                {pagesCount > 1 ? (
                    /* 
                        As for me, paging is not required for maximum 180 items
                    */
                    <div>
                        <ul className="pagination justify-content-center">
                            <li className="page-item">
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={e => {
                                        e.preventDefault();
                                        this.setState({
                                            pageId: 0
                                        });
                                    }}
                                >
                                    <i className="fa fa-angle-double-left" />
                                </a>
                            </li>

                            {Array(
                                pagesCount
                            )
                                .fill(0) // Tricky way to create a range
                                .map((_, pageId) => (
                                    <li
                                        className={classnames(
                                            "page-item",
                                            pageId === this.state.pageId
                                                ? "active"
                                                : ""
                                        )}
                                        key={`pageId-${pageId}`}
                                    >
                                        <a
                                            className="page-link"
                                            href="#"
                                            onClick={e => {
                                                e.preventDefault();
                                                this.setState({
                                                    pageId
                                                });
                                            }}
                                        >
                                            {pageId + 1}
                                        </a>
                                    </li>
                                ))}

                            <li className="page-item">
                                <a
                                    className="page-link"
                                    href="#"
                                    onClick={e => {
                                        e.preventDefault();
                                        this.setState({
                                            pageId: pagesCount - 1
                                        });
                                    }}
                                >
                                    <i className="fa fa-angle-double-right" />
                                </a>
                            </li>
                        </ul>
                    </div>
                ) : null}
            </div>
        );
    }
}
